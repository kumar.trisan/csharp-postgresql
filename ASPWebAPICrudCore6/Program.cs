using ASPWebAPICrudCore6.DB;
using ASPWebAPICrudCore6.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var provider=builder.Services.BuildServiceProvider();
var config=provider.GetRequiredService<IConfiguration>();
//builder.Services.AddDbContext<StudentDBContext>(item => item.UseSqlServer(config.GetConnectionString("Stu")));
//builder.Services.AddDbContext<psqldbcontext>(item => item.UseNpgsql(config.GetConnectionString("Stuposgresql")));
builder.Services.AddDbContext<Psqldbcontext>(options =>
       options.UseNpgsql(config.GetConnectionString("Stuposgresql")));



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
