﻿using ASPWebAPICrudCore6.Models;
using Microsoft.EntityFrameworkCore;
using System;


namespace ASPWebAPICrudCore6.DB
{
    public class Psqldbcontext : DbContext
    {
        public DbSet<Employee> employees { get; set; }

        public Psqldbcontext(DbContextOptions<Psqldbcontext> options)
            : base(options)
        {
        }
    }
}
