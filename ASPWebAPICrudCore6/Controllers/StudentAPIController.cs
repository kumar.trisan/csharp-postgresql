﻿using ASPWebAPICrudCore6.DB;
using ASPWebAPICrudCore6.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ASPWebAPICrudCore6.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentAPIController : ControllerBase
    {
        //private readonly StudentDBContext _context;
        private readonly Psqldbcontext _context;

        public StudentAPIController(Psqldbcontext context) 
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Employee>>> GetStudent()
        {
            var data = await _context.employees.ToListAsync();
            return Ok(data);

        }
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<StudentDetail>>> GetStudent()
        //{ 
        //var data=await _context.StudentDetails.ToListAsync();
        //    return Ok(data);

        //}



        //[HttpGet("{id}")]
        //public async Task<ActionResult<StudentDetail>> GetStudentById(int id)
        //{ 
        //var Student=await context.StudentDetails.FindAsync(id);
        //    if (Student==null)
        //    {
        //    return NotFound();

        //    }
        //    return Student;

        //}


    }
}
