﻿using System;
using System.Collections.Generic;

namespace ASPWebAPICrudCore6.Models
{
    public partial class StudentDetail
    {
        public int StuId { get; set; }
        public string StudentName { get; set; } = null!;
        public string StudentGender { get; set; } = null!;
        public int StuAge { get; set; }
        public int StuStandard { get; set; }
        public string StuFatherName { get; set; } = null!;
    }
}
