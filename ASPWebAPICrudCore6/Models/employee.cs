﻿using System.ComponentModel.DataAnnotations;

namespace ASPWebAPICrudCore6.Models
{
    // EmployeeService/Models/Employee.cs
    public class Employee
    {
        [Key] public int employee_id { get; set; }       
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string job_title { get; set; }
        public decimal salary { get; set; }
    }

}
